package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    private final EmployeeJPARepository employeeJPARepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, EmployeeJPARepository employeeJPARepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.employeeJPARepository = employeeJPARepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<Employee> findAll() {
        return employeeJPARepository.findAll();
    }


    public EmployeeResponse findById(Long id) {
        Employee employee = employeeJPARepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toResponse(employee);
    }
    public EmployeeResponse update(Long id, Employee employee) {
        EmployeeResponse toBeUpdatedEmployee = findById(id);
        Employee employeeTemp = new Employee();
        BeanUtils.copyProperties(toBeUpdatedEmployee, employeeTemp);

        if (employee.getSalary() != null) {
            employeeTemp.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            employeeTemp.setAge(employee.getAge());
        }
        Employee saveEmployee = employeeJPARepository.save(employeeTemp);
        return EmployeeMapper.toResponse(saveEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return employeeJPARepository.findByGender(gender);
    }

    public EmployeeResponse create(Employee employee) {
        Employee save = employeeJPARepository.save(employee);
        return EmployeeMapper.toResponse(save);
    }


    public List<Employee> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return employeeJPARepository.findAll(pageRequest).getContent();
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }

}
