#ORID

## O

- Flyway and Mapper: Mapper can help us map the fields required by our business to the fields in the database, which can reduce the exposure of privacy fields. Flyway can help us manage the database.
- Cloud Native: Today we learned about cloud native, but I am quite confused about these concepts because the afternoon time is quite tight and the lecture is not very detailed. This may require me to learn on my own after class.
- Retro: The Retro at noon was my first time participating, and I found it very interesting. My friends all talked about their feelings since the training from the bottom of their hearts. I thought that after entering the workplace, there would only be cold working relationships between colleagues. Now, the harmonious atmosphere between the teams has exceeded my expectations, which is great.

## R
Good

## I
I am not very familiar with the new knowledge I learned today, and I need to learn more.

## D
I will learn more about Spring Boot and JPA.